#!/bin/bash
# vi: et sts=4 sw=4 ts=4
WORKDIR=${0%/*}
OUT=$WORKDIR/weatherspect

printf 'Outputting to %s\n' "$OUT" >&2

shopt -s globstar
"$WORKDIR/util/perl-squasher/squash" \
    "$WORKDIR/weatherspect-main.pl" \
    "$WORKDIR"/lib/**/*.pm \
    > "$OUT"
chmod +x -- "$OUT"
