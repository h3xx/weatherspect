- Either get `Weather::Underground` working, or switch over to using
  `Weather::Bug`. The current `Weather::Underground` implementation does not
  work. Both services require user-provided API keys.
